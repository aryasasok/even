import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EvenNumbersTest {

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testEvenNumber() {
		
		EvenNumbers x= new EvenNumbers();
		boolean actualOutput = x.isEven(2);
		assertEquals(true, actualOutput);
	
	
	}
	@Test
	void testOddNumber()
	{
		EvenNumbers x= new EvenNumbers();
		boolean actualOutput = x.isEven(3);
		assertEquals(false, actualOutput);
	}
	
	@Test
	void testlessthanone()
	{
		EvenNumbers x= new EvenNumbers();
		boolean actualOutput = x.isEven(-1);
		assertEquals(false, actualOutput);
	}
	
}

